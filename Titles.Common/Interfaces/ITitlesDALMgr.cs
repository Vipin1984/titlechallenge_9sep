﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Titles.Common.Interfaces
{
    //Interface to manage data layer
    public interface ITitlesDALMgr
    {
        List<Title> SearchTitles(string searchTerm);

        TitlesDetailResult GetDetails(int id);
    }
}
