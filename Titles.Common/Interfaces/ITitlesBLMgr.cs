﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Titles.Common;

namespace Titles.Common.Interfaces
{
    //Interface to manage business layer
    public interface ITitlesBLMgr
    {
        List<Title> SearchTitles(string searchTerm);

        Titles.Common.TitlesDetailResult GetDetails(int id);
    }
}
