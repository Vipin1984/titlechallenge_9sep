﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Titles.Common
{
    // Datatype to get the particpants Name and Role
   public class ParticipantDetails
    {

            public string Name { get; set; }

            public string Role { get; set; }
       
    }
}
