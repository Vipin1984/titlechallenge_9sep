﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Titles.Common
{
    //Data type get the Title Details
    public class TitlesDetailResult
    {
        public List<Award> AwardSet
        { get; set; }

        public List<string> GenreSet
        { get; set; }

        public List<OtherName> OtherNameSet
        { get; set; }

        public List<StoryLine> StoryLineSet
        { get; set; }

        public List<ParticipantDetails> ParticipantSet
        { get; set; }
    }
}

