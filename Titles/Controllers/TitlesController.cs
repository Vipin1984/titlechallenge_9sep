﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Titles.Common.Interfaces;


namespace Titles.Controllers
{
    public class TitlesController : Controller
    {

        private ITitlesBLMgr _titleMgr { get; set; }
        

        public TitlesController()
        {
            _titleMgr = new Titles.BL.TitlesBusinessMgr();
        }       

        
        //Action Method return the Search view
        public ActionResult SearchTitles()
        {
            return View();
        }

        //Action Method returns the filtered search results
        [HttpPost]
        public PartialViewResult GetFilteredData(string term)
        {          

            var data =  _titleMgr.SearchTitles(term);
            var _titleModel = new Models.TitleModel();
            _titleModel.Titles=data;

            return PartialView("FilteredDataView", _titleModel);
        }

        //Action Method retuns the Title Detials
        public ActionResult TitleDetails(int id)
        {
          
            try
            {
               var _titleModel = _titleMgr.GetDetails(id);

                return PartialView(_titleModel);
            }
            catch (Exception)
            {                
                throw;
            }
        }

        


	}
}