﻿$(document).ready(function () {
    var ids = [];

    $("#TitleName").val("SEARCH BY TITLE NAME");

    $("#TitleName").focus(function () {
        var val = $("#TitleName").val();
        if (val == "SEARCH BY TITLE NAME")
        $("#TitleName").val("");
    });

    $("#TitleName").blur(function () {
        var val = $("#TitleName").val();
        if(!val)
            $("#TitleName").val("SEARCH BY TITLE NAME");
    });

    $("#TitleName").keyup(function () {
        var value = $(this).val();        
       
        if (!value) {//validate           
            return false;
        }

        $('#filteredData').find('div').html("Loading Data...");
        var ajaxid = $.ajax({
            url: '/Titles/GetFilteredData?term=' + value,
            type: 'POST',
            contentType: 'text/html',
            success: function (a) {
                ids.push(ajaxid);
                var ajaxObject = ids[ids.length - 2];
                try {
                    if (ajaxObject)
                        ids[ajaxObject].abort();
                } catch (e) {
                    //Do nothing
                }

                $('#filteredData').find('div').html(a);
                clickEvent();
                $('#filteredData').show();
            },
            error: function (c, d) {
                //Handle
            }
        });

    });
});

function clickEvent() {
    $('.detailsLink').unbind('click');
    $('.detailsLink').click(function () {
        $('#detailsField').fadeIn('slow');
        $('#detailsContainer').html("<a style='color:white'>Loading Data...</a>")
        var value = $(this).attr('value');
        if (isNaN(value))
            return;
        var ajaxid = $.ajax({
            url: '/Titles/TitleDetails?id=' + value,
            type: 'POST',
            contentType: 'application/json',
            success: function (a) {
                $('#detailsContainer').html(a);

            },
            error: function (c, d) {
                debugger;
            }
        });
    });

}
