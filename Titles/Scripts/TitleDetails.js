﻿$(document).ready(function () {


    $("#accordion h2").find("span").addClass('glyphicon glyphicon-chevron-right');
    $("#accordion h2").next().addClass('displayNone');
    $("#accordion h2").click(function () {


        if ($(this).find("span").attr("class") == 'glyphicon glyphicon-chevron-right') {
            $("#accordion h2").find("span").removeClass();
            $("#accordion h2").find("span").addClass('glyphicon glyphicon-chevron-right');
            $(this).find("span").removeClass('glyphicon glyphicon-chevron-right');
            $(this).find("span").addClass('glyphicon glyphicon-chevron-down');
        }

        else {
            $("#accordion h2").find("span").removeClass();
            $("#accordion h2").find("span").addClass('glyphicon glyphicon-chevron-right');

        }

        var context = $(this).next();//div object;
        if (!context.hasClass('displayNone')) { // If not applied apply the class
            $("#accordion h2").next().addClass('displayNone');
            return;
        }
        $("#accordion h2").next().addClass('displayNone');
        context.removeClass('displayNone');
        $("#accordion h2").next().addClass('detailContents');




    });
});