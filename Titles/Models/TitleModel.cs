﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Titles.Common;

namespace Titles.Models
{
    public class TitleModel
    {

        public TitleModel()
        {
            Titles = new List<Title>();
        }
        
        
        public List<Title> Titles { get; set; }


    }
}