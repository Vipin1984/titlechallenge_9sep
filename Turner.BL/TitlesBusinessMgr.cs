﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Titles.Common;
using Titles.Common.Interfaces;

namespace Titles.BL
{
    /// <summary>
    /// 
    /// </summary>
    public class TitlesBusinessMgr : ITitlesBLMgr
    {
        //Business Access Layer
        private ITitlesDALMgr _dataManager { get; set; }


        public TitlesBusinessMgr()
        {
            _dataManager = new Titles.DAL.TitleDataMgr();
        }

        public List<Title> SearchTitles(string searchTerm)
        {            
            try
            {
                return _dataManager.SearchTitles(searchTerm);
            }
            catch (Exception)
            {                
                throw;
            }
        }


        public Titles.Common.TitlesDetailResult GetDetails(int id)
        {
            try
            {
                return _dataManager.GetDetails(id);
            }
            catch (Exception)
            {
                
                throw;
            }
        }
    }
}
