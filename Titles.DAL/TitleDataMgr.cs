﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Titles.Common;
using Titles.Common.Interfaces;

namespace Titles.DAL
{
    //Data Access Layer
    public class TitleDataMgr : ITitlesDALMgr
    {

        private Titles.Common.TitlesDB _titleDB { get; set; }

        public TitleDataMgr()
        {
            _titleDB = new Titles.Common.TitlesDB();
        }

        //To get the search results based on search key
        public List<Title> SearchTitles(string searchTerm)
        {
            var titles = new List<Title>();
            try
            {
                var resultset = (from t in _titleDB.Titles where t.TitleName.Contains(searchTerm) select t);
                if (resultset != null)
                {
                    foreach (var item in resultset)
                    {
                        titles.Add(new Title()
                        {
                            TitleName = item.TitleName,
                            TitleId = item.TitleId
                        });
                    }
                }
                return titles;
            }
            catch (Exception ex )        
            {                
                throw ex;
            }
        }

        //To get the detail resulsts based on the Title Id
        public TitlesDetailResult GetDetails(int id)
        {
            var _details = new TitlesDetailResult();
            try
            {
                var _AwardsSet = (from a in _titleDB.Awards where a.TitleId.Equals(id) select a);             

                var _GenreSet = from s in _titleDB.TitleGenres
                               join c in _titleDB.Genres on s.GenreId equals c.Id
                               where s.TitleId == id
                               select c.Name;

                var _ParticipantSet = (from s in _titleDB.TitleParticipants
                                      join c in _titleDB.Participants on s.ParticipantId equals c.Id
                                      where s.TitleId == id&& s.IsKey
                                      select new ParticipantDetails { Name = c.Name, Role = s.RoleType }).Distinct().ToList();
                                                
                var _OthernameSet = (from a in _titleDB.OtherNames where a.TitleId.Equals(id) select a);

                var _StorylineSet = (from a in _titleDB.StoryLines where a.TitleId.Equals(id) select a);

               _details.AwardSet = _AwardsSet.ToList();
               _details.GenreSet = _GenreSet.ToList();
               _details.ParticipantSet = _ParticipantSet.ToList();
               _details.OtherNameSet = _OthernameSet.ToList();
               _details.StoryLineSet = _StorylineSet.ToList();

               return (_details);
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
    }
}
